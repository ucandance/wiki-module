<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;
use Finnito\WikiModule\Category\CategoryModel;

class FinnitoModuleWikiCreateWikiFields extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $fields = [
        /* Shared Fields */
        'name' => 'anomaly.field_type.text',
        'slug' => [
            'type' => 'anomaly.field_type.slug',
            'config' => [
                'slugify' => 'name',
                'type' => '-'
            ],
        ],
        "commit" => [
            "type" => "anomaly.field_type.textarea",
            "config" => [
                "rows" => 4,
                "max" => 256,
                "show_counter" => true,
            ],
        ],

        /* Wiki Entry-Only Fields */
        "category" => [
            "type" => "anomaly.field_type.relationship",
            "config" => [
                "related" => CategoryModel::class,
                "mode" => "dropdown",
            ],
        ],
        "content" => [
            "type" => "anomaly.field_type.wysiwyg",
            "config" => [
                "height" => 500,
            ],
        ],

        /* Category-Only Fields */
        "description" => [
            "type" => "anomaly.field_type.textarea",
            "config" => [
                "rows" => 6,
                "max" => 256,
                "show_counter" => true,
            ],
        ],
    ];

}
