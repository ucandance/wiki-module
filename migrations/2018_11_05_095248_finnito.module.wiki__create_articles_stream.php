<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class FinnitoModuleWikiCreateArticlesStream extends Migration
{

    /**
     * The stream definition.
     *
     * @var array
     */
    protected $stream = [
        'slug' => 'articles',
         'title_column' => 'name',
         'translatable' => false,
         'versionable' => true,
         'trashable' => false,
         'searchable' => true,
         'sortable' => true,
    ];

    /**
     * The stream assignments.
     *
     * @var array
     */
    protected $assignments = [
        'name' => [
            'required' => true,
        ],
        'slug' => [
            'unique' => true,
            'required' => true,
        ],
        "category" => [
            "required" => true,
        ],
        "content",
        "commit" => [
            "required" => true,
        ],
    ];

}
