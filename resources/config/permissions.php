<?php

return [
    'categories' => [
        'read',
        'write',
        'delete',
    ],
    'articles' => [
        'read',
        'write',
        'delete',
    ],
];
