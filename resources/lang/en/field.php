<?php

return [
	"name.name" => "Name",
	"slug.name" => "Slug",
	"description.name" => "Description",
	"category.name" => "Category",
	"content.name" => "Content",
	"commit" => [
		"name" => "Change Description",
		"instructions" => "Briefly describe what you changed.",
	],
];
