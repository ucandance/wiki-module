<?php

return [
    'categories' => [
        'name'   => 'Categories',
        'option' => [
            'read'   => 'Can read categories?',
            'write'  => 'Can create/edit categories?',
            'delete' => 'Can delete categories?',
        ],
    ],
    'articles' => [
        'name'   => 'Articles',
        'option' => [
            'read'   => 'Can read articles?',
            'write'  => 'Can create/edit articles?',
            'delete' => 'Can delete articles?',
        ],
    ],
];
