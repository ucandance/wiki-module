<?php namespace Finnito\WikiModule\Article;

use Finnito\WikiModule\Article\Contract\ArticleRepositoryInterface;
use Anomaly\Streams\Platform\Entry\EntryRepository;

class ArticleRepository extends EntryRepository implements ArticleRepositoryInterface
{

    /**
     * The entry model.
     *
     * @var ArticleModel
     */
    protected $model;

    /**
     * Create a new ArticleRepository instance.
     *
     * @param ArticleModel $model
     */
    public function __construct(ArticleModel $model)
    {
        $this->model = $model;
    }

    public function findByCategoryIDAndArticleSlug($categoryID, $articleSlug)
    {
        return $this->model
            ->where('category_id', $categoryID)
            ->where("slug", $articleSlug)
            ->first();
    }
}
