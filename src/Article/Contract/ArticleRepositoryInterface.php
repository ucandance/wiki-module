<?php namespace Finnito\WikiModule\Article\Contract;

use Anomaly\Streams\Platform\Entry\Contract\EntryRepositoryInterface;

interface ArticleRepositoryInterface extends EntryRepositoryInterface
{
    public function findByCategoryIdAndArticleSlug($categoryID, $articleSlug);
}
