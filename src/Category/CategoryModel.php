<?php namespace Finnito\WikiModule\Category;

use Finnito\WikiModule\Category\Contract\CategoryInterface;
use Anomaly\Streams\Platform\Model\Wiki\WikiCategoriesEntryModel;
use Finnito\WikiModule\Article\ArticleModel;

class CategoryModel extends WikiCategoriesEntryModel implements CategoryInterface
{
	public function articles()
	{
		return $this->hasMany(ArticleModel::class, "category_id")->get();
	}
}
