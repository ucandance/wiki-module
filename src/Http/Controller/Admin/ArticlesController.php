<?php namespace Finnito\WikiModule\Http\Controller\Admin;

use Finnito\WikiModule\Article\Form\ArticleFormBuilder;
use Finnito\WikiModule\Article\Table\ArticleTableBuilder;
use Anomaly\Streams\Platform\Http\Controller\AdminController;

class ArticlesController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param ArticleTableBuilder $table
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index(ArticleTableBuilder $table)
    {
        return $table->render();
    }

    /**
     * Create a new entry.
     *
     * @param ArticleFormBuilder $form
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function create(ArticleFormBuilder $form)
    {
        return $form->render();
    }

    /**
     * Edit an existing entry.
     *
     * @param ArticleFormBuilder $form
     * @param        $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function edit(ArticleFormBuilder $form, $id)
    {
        return $form->render($id);
    }
}
