<?php namespace Finnito\WikiModule\Http\Controller;

use Finnito\WikiModule\Category\Contract\CategoryRepositoryInterface;
use Finnito\WikiModule\Article\Contract\ArticleRepositoryInterface;
use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Illuminate\Contracts\Auth\Guard;

/**
 * Class WikiController
 *
 * @link          https://finnito.nz/
 * @author        Finn LeSueur <finn.lesueur@gmail.com>
 */
class WikiController extends PublicController
{

    /**
     * Return an index of all wiki articles.
     *
     * @param CategoryRepositoryInterface $categories
     * @param Guard $auth
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function index(CategoryRepositoryInterface $categories, Guard $auth)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        $allCategories = $categories->all();
        $this->template->set("meta_title", "Wiki");
        $this->breadcrumbs->add("Wiki", "/wiki");
        return $this->view->make(
            "finnito.module.wiki::public/index",
            [
                "categories" => $allCategories,
            ]
        );
    }

    /**
     * Return an index of all wiki articles for
     * a given category.
     *
     * @param CategoryRepositoryInterface $categories
     * @param Guard $auth
     * @param $categorySlug
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function category(CategoryRepositoryInterface $categories, Guard $auth, $categorySlug)
    {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        if (!$category = $categories->findBy("slug", $categorySlug)) {
            abort(404);
        }

        $allCategories = $categories->all();

        $this->breadcrumbs->add("Wiki", "/wiki");
        $this->breadcrumbs->add($category->name, $this->request->path());
        $this->template->set('edit_link', "/admin/wiki/categories/edit/".$category->id);
        $this->template->set("edit_type", $category->name);
        $this->template->set("meta_title", $category->name);

        return $this->view->make(
            "finnito.module.wiki::public/category",
            [
                "categories" => $allCategories,
                "category" => $category,
            ]
        );
    }

    /**
     * Return a single wiki article.
     *
     * @param ArticleRepositoryInterface $articles
     * @param CategoryRepositoryInterface $categories
     * @param Guard $auth
     * @param $categorySlug
     * @param $articleSlug
     * @return \Illuminate\Contracts\View\View|mixed
     */
    public function article(
        ArticleRepositoryInterface $articles,
        CategoryRepositoryInterface $categories,
        Guard $auth,
        $categorySlug,
        $articleSlug
    ) {
        if (!$user = $auth->user()) {
            redirect('/login?redirect='.url()->previous())->send();
        }

        if (!$user->hasAnyRole(["admin", "committee", "volunteer"])) {
            abort(403);
        }

        if (!$category = $categories->findBy("slug", $categorySlug)) {
            abort(404);
        }

        if (!$article = $articles->findByCategoryIdAndArticleSlug($category->id, $articleSlug)) {
            abort(404);
        }
        $this->breadcrumbs->add("Wiki", "/wiki");
        $this->breadcrumbs->add($category->name, "/wiki/{$category->slug}");
        $this->breadcrumbs->add($article->name, $this->request->path());
        $this->template->set('edit_link', "/admin/wiki/edit/".$article->id);
        $this->template->set("edit_type", $article->name);
        $this->template->set("meta_title", $article->name);
        return $this->view->make(
            "finnito.module.wiki::public/article",
            [
                "article" => $article,
                "category" => $category,
            ]
        );
    }
}
