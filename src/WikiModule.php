<?php namespace Finnito\WikiModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class WikiModule extends Module
{

    /**
     * The navigation display flag.
     *
     * @var bool
     */
    protected $navigation = true;

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-question';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'articles' => [
            'buttons' => [
                'new_article',
            ],
        ],
        'categories' => [
            'buttons' => [
                'new_category',
            ],
        ],
    ];

}
