<?php namespace Finnito\WikiModule;

use Anomaly\Streams\Platform\Addon\AddonServiceProvider;
use Finnito\WikiModule\Article\Contract\ArticleRepositoryInterface;
use Finnito\WikiModule\Article\ArticleRepository;
use Anomaly\Streams\Platform\Model\Wiki\WikiArticlesEntryModel;
use Finnito\WikiModule\Article\ArticleModel;
use Finnito\WikiModule\Category\Contract\CategoryRepositoryInterface;
use Finnito\WikiModule\Category\CategoryRepository;
use Anomaly\Streams\Platform\Model\Wiki\WikiCategoriesEntryModel;
use Finnito\WikiModule\Category\CategoryModel;
use Illuminate\Routing\Router;

class WikiModuleServiceProvider extends AddonServiceProvider
{

    /**
     * Additional addon plugins.
     *
     * @type array|null
     */
    protected $plugins = [];

    /**
     * The addon Artisan commands.
     *
     * @type array|null
     */
    protected $commands = [];

    /**
     * The addon's scheduled commands.
     *
     * @type array|null
     */
    protected $schedules = [];

    /**
     * The addon API routes.
     *
     * @type array|null
     */
    protected $api = [];

    /**
     * The addon routes.
     *
     * @type array|null
     */
    protected $routes = [
        'admin/wiki'           => 'Finnito\WikiModule\Http\Controller\Admin\ArticlesController@index',
        'admin/wiki/create'    => 'Finnito\WikiModule\Http\Controller\Admin\ArticlesController@create',
        'admin/wiki/edit/{id}' => 'Finnito\WikiModule\Http\Controller\Admin\ArticlesController@edit',
        'admin/wiki/categories'           => 'Finnito\WikiModule\Http\Controller\Admin\CategoriesController@index',
        'admin/wiki/categories/create'    => 'Finnito\WikiModule\Http\Controller\Admin\CategoriesController@create',
        'admin/wiki/categories/edit/{id}' => 'Finnito\WikiModule\Http\Controller\Admin\CategoriesController@edit',

        "wiki" => "Finnito\WikiModule\Http\Controller\WikiController@index",
        "wiki/{category}" => "Finnito\WikiModule\Http\Controller\WikiController@category",
        "wiki/{category}/{article}" => "Finnito\WikiModule\Http\Controller\WikiController@article",
    ];

    /**
     * The addon middleware.
     *
     * @type array|null
     */
    protected $middleware = [
        //Finnito\WikiModule\Http\Middleware\ExampleMiddleware::class
    ];

    /**
     * Addon group middleware.
     *
     * @var array
     */
    protected $groupMiddleware = [
        //'web' => [
        //    Finnito\WikiModule\Http\Middleware\ExampleMiddleware::class,
        //],
    ];

    /**
     * Addon route middleware.
     *
     * @type array|null
     */
    protected $routeMiddleware = [];

    /**
     * The addon event listeners.
     *
     * @type array|null
     */
    protected $listeners = [
        //Finnito\WikiModule\Event\ExampleEvent::class => [
        //    Finnito\WikiModule\Listener\ExampleListener::class,
        //],
    ];

    /**
     * The addon alias bindings.
     *
     * @type array|null
     */
    protected $aliases = [
        //'Example' => Finnito\WikiModule\Example::class
    ];

    /**
     * The addon class bindings.
     *
     * @type array|null
     */
    protected $bindings = [
        WikiArticlesEntryModel::class => ArticleModel::class,
        WikiCategoriesEntryModel::class => CategoryModel::class,
    ];

    /**
     * The addon singleton bindings.
     *
     * @type array|null
     */
    protected $singletons = [
        ArticleRepositoryInterface::class => ArticleRepository::class,
        CategoryRepositoryInterface::class => CategoryRepository::class,
    ];

    /**
     * Additional service providers.
     *
     * @type array|null
     */
    protected $providers = [
        //\ExamplePackage\Provider\ExampleProvider::class
    ];

    /**
     * The addon view overrides.
     *
     * @type array|null
     */
    protected $overrides = [
        //'streams::errors/404' => 'module::errors/404',
        //'streams::errors/500' => 'module::errors/500',
    ];

    /**
     * The addon mobile-only view overrides.
     *
     * @type array|null
     */
    protected $mobile = [
        //'streams::errors/404' => 'module::mobile/errors/404',
        //'streams::errors/500' => 'module::mobile/errors/500',
    ];

    /**
     * Register the addon.
     */
    public function register()
    {
        // Run extra pre-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Boot the addon.
     */
    public function boot()
    {
        // Run extra post-boot registration logic here.
        // Use method injection or commands to bring in services.
    }

    /**
     * Map additional addon routes.
     *
     * @param Router $router
     */
    public function map(Router $router)
    {
        // Register dynamic routes here for example.
        // Use method injection or commands to bring in services.
    }

}
